## DataSearched
   
    Поиск и фильтрация данных (Laravel8 Vue.js HTML ElementUI)

Форма поиска, которая запрашивает API с помощью AJAX и отображает результаты, полученные от серверной части. 
Результат поиска отображается в таблице HTML динамически с использованием Vue.js.
Покрытие тестами: unit, интеграционные

#### Технологии

* Laravel 8
* VueJS
* ElementUI


#### Установка
1. Склонировать проект **datasearched**: `git clone https://bitbucket.org/portfolio-dev/datasearched.git`
2. Перейти в проект: `cp datasearched`
3. Файл .env.example скопировать и переименовать в .env. 
В нем указать доступы к БД


#### Запуск
 * ##### с помощью Docker
 

 * ##### без Docker
	1. Установить Composer зависимости: `php composer install`
	2. Сгенерировать ключ: `php artisan key:generate`
	3. Запустить в БД миграции: `php artisan migrate`
	4. Запустить заполнение данными БД: 
	 - `php artisan db:seed --class=DatabaseSeeder`
     - или
     - `php artisan migrate:fresh --seed`
     - или загрузить из CSV-файла 
     - `php artisan db:seed --class=ApartmentCSVSeeder`

    5. Скомпилировать проект: npm run dev    	
	6. Запустить сервер: `php artisan serve --port=8000`
	

#### Проверка API
    headers `Accept: application/json`
    
    * Список всех данных
        method: `get` 
        url: http://localhost:8000/api/apartment/show
    
    * Поиск по названию
        method: `post`
        content-Type: application/x-www-form-urlencoded
        url: http://localhost:8000/api/apartment/filter/name
        body: name
        
    * Поиск по цене, диапазон (от min до max)
        method: `post`
        content-Type: application/x-www-form-urlencoded
        url: http://localhost:8000/api/apartment/filter/price
        body: min, max    

    * Поиск по номеру комнат 
        method: `post`
        content-Type: application/x-www-form-urlencoded
        url: http://localhost:8000/api/apartment/filter/rooms
        body: number, field    
        где field соответсвующее поле в таблице, может быть bedrooms, bathrooms, storeys или garages


#### Unit тестирование
запустить тесты `php artisan test`

#### Визуализация
![Alt text](public/screen.png?raw=true "фильтр")
