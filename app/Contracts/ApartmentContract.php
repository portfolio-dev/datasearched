<?php

declare(strict_types=1);

namespace App\Contracts;

/**
 * Interface ApartmentContract
 * @package App\Contracts
 */
interface ApartmentContract
{
    /**
     * @return mixed
     */
    public function showApartment();

    /**
     * @return mixed
     */
  /*  public function getPrices();*/

    /**
     * @param array $params
     * @return mixed
     */
    public function filterName(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function filterPrice(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function filterRooms(array $params);

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name);

    /**
     * @param int $min
     * @param int $max
     * @return mixed
     */
    public function findByPrice(int $min, int $max);

    /**
     * @param int $number
     * @param string $field
     * @return mixed
     */
    public function findByRooms(int $number, string $field);

    /**
     * @return mixed
     */
    public function getMinPrice();

    /**
     * @return mixed
     */
    public function getMaxPrice();
}
