<?php

declare(strict_types=1);

namespace App\Contracts;

/**
 * Interface BaseContract
 * @package App\Contracts
 */
interface BaseContract
{
    /**
     * @param array $data
     * @param int $code
     * @return mixed
     */
    public function response(array $data, int $code);
}
