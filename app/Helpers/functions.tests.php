<?php

if (!function_exists("getRulesFormRequests")) {
    /**
     * @param \Illuminate\Foundation\Http\FormRequest $formRequest
     * @return mixed
     */
    function getRulesFormRequests(\Illuminate\Foundation\Http\FormRequest $formRequest)
    {
        return $formRequest->rules();
    }
}
