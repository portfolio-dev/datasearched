<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Requests\FilterNameRequest;
use App\Http\Requests\FilterPriceRequest;
use App\Http\Requests\FilterRoomsRequest;
use App\Models\Apartment;
use App\Repositories\ApartmentRepository;
use Illuminate\Http\Request;

/**
 * Class ApartmentController
 * @package App\Http\Controllers\API
 */
class ApartmentController extends BaseController
{
    /** @var ApartmentRepository $repositoryApartment */
    protected $repositoryApartment;

    /**
     * ApartmentController constructor.
     */
    public function __construct()
    {
        $this->repositoryApartment = new ApartmentRepository(new Apartment());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $response = $this->repositoryApartment->showApartment();

        return $this->responseJson($response['data'], $response['code']);
    }

    /**
     * @param FilterNameRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterName(FilterNameRequest $request)
    {
        $params = $request->only('name');

        $response = $this->repositoryApartment->filterName($params);

        return $this->responseJson($response['data'], $response['code']);
    }

    /**
     * @param FilterPriceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterPrice(FilterPriceRequest $request)
    {
        $params = $request->only('min', 'max');

        $response = $this->repositoryApartment->filterPrice($params);

        return $this->responseJson($response['data'], $response['code']);
    }

    /**
     * @param FilterRoomsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterRooms(FilterRoomsRequest $request)
    {
        $params = $request->only('number', 'field');

        $response = $this->repositoryApartment->filterRooms($params);

        return $this->responseJson($response['data'], $response['code']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function minPrice(Request $request)
    {
        $response = $this->repositoryApartment->getMinPrice();

        return $this->responseJson($response['data'], $response['code']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function maxPrice(Request $request)
    {
        $response = $this->repositoryApartment->getMaxPrice();

        return $this->responseJson($response['data'], $response['code']);
    }
}
