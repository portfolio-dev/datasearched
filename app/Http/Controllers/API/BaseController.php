<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller as Controller;

/**
 * Class BaseController
 * @package App\Http\Controllers\API
 */
class BaseController extends Controller
{
    /**
     * @param array|null $data
     * @param int $responseCode
     * @return JsonResponse
     */
    protected function responseJson(array $data = null, int $responseCode = 200)
    {
        return response()->json([
            'data'          => $data,
            'response_code' => $responseCode
        ]);
    }
}
