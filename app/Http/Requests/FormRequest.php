<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;

abstract class FormRequest extends LaravelFormRequest
{
    const BAD_REQUEST = JsonResponse::HTTP_BAD_REQUEST;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $this->prepareFromArray((new ValidationException($validator))->errors());

        throw new HttpResponseException(
            response()->json([
                'response_code' => self::BAD_REQUEST,
                'errors' => $errors
            ], self::BAD_REQUEST)
        );
    }

    /**
     * Convert array to string
     *
     * @param array $params
     * @return array
     */
    private function prepareFromArray(array $params)
    {
        $result = [];

        foreach ($params as $key => $param) {
            $result[$key] = implode('; ', array_map(function ($entry) {
                return $entry;
            }, $param));
        }

        return $result;
    }
}
