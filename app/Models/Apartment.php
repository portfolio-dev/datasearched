<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apartment extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'apartments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'bedrooms',
        'bathrooms',
        'storeys',
        'garages',
    ];

    /**
     * @param $query
     * @param string $name
     * @return mixed
     */
    public function scopeOfName($query, string $name)
    {
        return $query->where('name', 'like', '%'.$name.'%');
    }

    /**
     * @param $query
     * @param int $min
     * @param int $max
     * @return mixed
     */
    public function scopeOfPrice($query, int $min, int $max)
    {
        return $query->whereBetween('price', [$min, $max]);
    }

    /**
     * @param $query
     * @param int $number
     * @param string $field
     * @return mixed
     */
    public function scopeOfRooms($query, int $number, string $field)
    {
        return $query->where($field, $number);
    }
}
