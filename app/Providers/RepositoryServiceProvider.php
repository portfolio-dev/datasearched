<?php

declare(strict_types=1);

namespace App\Providers;

use App\Contracts\ApartmentContract;
use App\Repositories\ApartmentRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /** @var array $repositories */
    protected $repositories = [
        ApartmentContract::class => ApartmentRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        foreach ($this->repositories as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }
}
