<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\ApartmentContract;
use App\Http\Resources\ApartmentCollection;
use App\Models\Apartment;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ApartmentRepository
 * @package App\Repositories
 */
class ApartmentRepository extends BaseRepository implements ApartmentContract
{
    /**
     * ApartmentRepository constructor.
     * @param Apartment $model
     */
    public function __construct(Apartment $model)
    {
        parent::__construct($model);

        $this->model = $model;
    }

    /**
     * @return array|mixed
     */
    public function showApartment()
    {
        try {
            $apartments = ApartmentCollection::collection($this->findAll());

            if (is_null($apartments) || count($apartments) === 0) {
                $code = self::UNAUTHORISED_STATUS_CODE;
                $data = [
                    'error'     => true,
                    'message'   => 'Apartments not found',
                ];
            } else {
                $code = self::SUCCESS_STATUS_CODE;
                $data = [
                    'error'         => false,
                    'message'       => 'Apartments show successfully',
                    'apartments'    => $apartments,
                ];
            }
        } catch (\Exception $e) {
            $code = self::UNAUTHORISED_STATUS_CODE;
            $data = [
                'error'     => true,
                'message'   => 'Apartments not found',
                'exception' => $e->getMessage(),
            ];
        }

        return $this->response($data, $code);
    }

    /**
     * @param array $params
     * @return array|mixed
     */
    public function filterName(array $params)
    {
        try {
            /** @var array $apartments */
            $apartments = ApartmentCollection::collection($this->findByName($params['name']));

            if (is_null($apartments) || count($apartments) === 0) {
                $code = self::UNAUTHORISED_STATUS_CODE;
                $data = [
                    'error'     => true,
                    'message'   => 'Apartments not found',
                ];
            } else {
                $code = self::SUCCESS_STATUS_CODE;
                $data = [
                    'error'         => false,
                    'message'       => 'Apartments find successfully',
                    'apartments'    => $apartments,
                ];
            }
        } catch (\Exception $e) {
            $code = self::UNAUTHORISED_STATUS_CODE;
            $data = [
                'error'     => true,
                'message'   => 'Apartments not found',
                'exception' => $e->getMessage(),
            ];
        }

        return $this->response($data, $code);
    }

    /**
     * @param array $params
     * @return array|mixed
     */
    public function filterPrice(array $params)
    {
        try {
            /** @var array $apartments */
            $apartments = ApartmentCollection::collection($this->findByPrice((int)$params['min'], (int)$params['max']));

            if (is_null($apartments) || count($apartments) === 0) {
                $code = self::UNAUTHORISED_STATUS_CODE;
                $data = [
                    'error'     => true,
                    'message'   => 'Apartments not found',
                ];
            } else {
                $code = self::SUCCESS_STATUS_CODE;
                $data = [
                    'error'         => false,
                    'message'       => 'Apartments find successfully',
                    'apartments'    => $apartments,
                ];
            }
        } catch (\Exception $e) {
            $code = self::UNAUTHORISED_STATUS_CODE;
            $data = [
                'error'     => true,
                'message'   => 'Apartments not found',
                'exception' => $e->getMessage(),
            ];
        }

        return $this->response($data, $code);
    }

    /**
     * @param array $params
     * @return array|mixed
     */
    public function filterRooms(array $params)
    {
        try {
            /** @var array $apartments */
            $apartments = ApartmentCollection::collection($this->findByRooms((int)$params['number'], $params['field']));

            if (is_null($apartments) || count($apartments) === 0) {
                $code = self::UNAUTHORISED_STATUS_CODE;
                $data = [
                    'error'     => true,
                    'message'   => 'Apartments not found',
                ];
            } else {
                $code = self::SUCCESS_STATUS_CODE;
                $data = [
                    'error'         => false,
                    'message'       => 'Apartments find successfully',
                    'apartments'    => $apartments,
                ];
            }
        } catch (\Exception $e) {
            $code = self::UNAUTHORISED_STATUS_CODE;
            $data = [
                'error'     => true,
                'message'   => 'Apartments not found',
                'exception' => $e->getMessage(),
            ];
        }

        return $this->response($data, $code);
    }

    /**
     * @return array|mixed
     */
    public function getMinPrice()
    {
        try {
            /** @var array $apartments */
            $apartments = Apartment::all('price')->toArray();

            if (is_null($apartments)) {
                $code = self::UNAUTHORISED_STATUS_CODE;
                $data = [
                    'error'     => true,
                    'message'   => 'Prices not found',
                ];
            } else {
                /** @var array $prices */
                $prices = array_column($apartments, 'price');

                $code = self::SUCCESS_STATUS_CODE;
                $data = [
                    'error'         => false,
                    'message'       => 'Min price show successfully',
                    'min'           => min($prices),
                ];
            }
        } catch (\Exception $e) {
            $code = self::UNAUTHORISED_STATUS_CODE;
            $data = [
                'error'     => true,
                'message'   => 'Apartments not found',
                'exception' => $e->getMessage(),
            ];
        }

        return $this->response($data, $code);
    }

    /**
     * @return array|mixed
     */
    public function getMaxPrice()
    {
        try {
            /** @var array $apartments */
            $apartments = Apartment::all('price')->toArray();

            if (is_null($apartments)) {
                $code = self::UNAUTHORISED_STATUS_CODE;
                $data = [
                    'error'     => true,
                    'message'   => 'Prices not found',
                ];
            } else {
                /** @var array $prices */
                $prices = array_column($apartments, 'price');

                $code = self::SUCCESS_STATUS_CODE;
                $data = [
                    'error'         => false,
                    'message'       => 'Max price show successfully',
                    'max'           => max($prices),
                ];
            }
        } catch (\Exception $e) {
            $code = self::UNAUTHORISED_STATUS_CODE;
            $data = [
                'error'     => true,
                'message'   => 'Apartments not found',
                'exception' => $e->getMessage(),
            ];
        }

        return $this->response($data, $code);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name)
    {
        return $this->model->ofName($name)->get();
    }

    /**
     * @param int $min
     * @param int $max
     * @return mixed
     */
    public function findByPrice(int $min, int $max)
    {
        return $this->model->ofPrice($min, $max)->get();
    }

    /**
     * @param int $number
     * @param string $field
     * @return mixed
     */
    public function findByRooms(int $number, string $field)
    {
        return $this->model->ofRooms($number, $field)->get();
    }
}
