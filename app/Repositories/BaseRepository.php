<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\BaseContract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 * @package App\Repositories
 */
class BaseRepository implements BaseContract
{
    /** @var int */
    const SUCCESS_STATUS_CODE = 200;

    /** @var int */
    const UNAUTHORISED_STATUS_CODE = 401;

    /** @var Model */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     * @param int $code
     * @return array|mixed
     */
    public function response(array $data, int $code)
    {
        return [
            'data' => $data,
            'code' => $code,
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function findAll()
    {
        return $this->model->all();
    }
}
