<?php

namespace Database\Factories;

use App\Models\Apartment;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApartmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Apartment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'      => $this->faker->name,
            'price'     => $this->faker->randomFloat(2, 1000, 600000),
            'bedrooms'  => $this->faker->randomDigitNotNull,
            'bathrooms' => $this->faker->randomDigitNotNull,
            'storeys'   => $this->faker->randomDigitNotNull,
            'garages'   => $this->faker->randomDigitNotNull,
        ];
    }
}
