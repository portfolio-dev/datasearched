<?php

namespace Database\Seeders;

use App\Models\Apartment;
use Illuminate\Database\Seeder;

class ApartmentCSVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var array $apartments */
        $apartments = array_map('str_getcsv', file(storage_path('app/csv/apartment.csv')));

        foreach ($apartments as $item) {
            /** @var array $apartment */
            $apartment = explode(';', $item[0]);

            Apartment::create([
                'name'      => $apartment[0],
                'price'     => $apartment[1],
                'bedrooms'  => $apartment[2],
                'bathrooms' => $apartment[3],
                'storeys'   => $apartment[4],
                'garages'   => $apartment[5],
            ]);
        }
    }
}
