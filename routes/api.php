<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\ApartmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'API'], function () {
    Route::group(['prefix' => 'apartment'], function () {
        Route::get('show', [ApartmentController::class, 'show'])->name('apartment.show');

        Route::group(['prefix' => 'prices'], function () {
            Route::get('min', [ApartmentController::class, 'minPrice'])->name('apartment.prices.min');
            Route::get('max', [ApartmentController::class, 'maxPrice'])->name('apartment.prices.max');
        });

        Route::group(['prefix' => 'filter'], function () {
            Route::post('name', [ApartmentController::class, 'filterName']);
            Route::post('price', [ApartmentController::class, 'filterPrice']);
            Route::post('rooms', [ApartmentController::class, 'filterRooms']);
        });
    });
});
