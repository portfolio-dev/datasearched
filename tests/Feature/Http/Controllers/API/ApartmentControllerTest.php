<?php

namespace Tests\Feature\Http\Controllers\API;

use Tests\TestCase;

class ApartmentControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function testShow()
    {
        $response = $this->json('GET', '/api/apartment/show');

        $response
            ->assertJsonCount(2)
            ->assertJsonStructure([
                    "data" => [
                        "error",
                        "message",
                        "apartments" => [
                            ["id", "name", "price", "bedrooms", "bathrooms", "storeys", "garages"]
                        ]
                    ],
                    "response_code"
                ]
            )
            ->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testFilterNameRequiredFields()
    {
        $response = $this->json('POST', 'api/apartment/filter/name', ['Accept' => 'application/json']);

        $response
            ->assertJson([
                "errors" => [
                    "name" => "Необходимо указать name"
                ]
            ])
            ->assertStatus(400);
    }

    /** @return void */
    public function testFilterNameSuccessful()
    {
        $data = [
            'name' => 'a'
        ];

        $response = $this->json('POST', 'api/apartment/filter/name', $data, ['Accept' => 'application/json']);

        $response
            ->assertJsonStructure([
                'data' => [
                    "apartments" => [
                        ['id', 'name', 'price', 'bedrooms', 'bathrooms', 'storeys', 'garages']
                    ]
                ]
            ])
            ->assertStatus(200);
    }
}
