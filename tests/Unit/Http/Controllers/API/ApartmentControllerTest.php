<?php

namespace Tests\Unit\Http\Controllers\API;

use App\Http\Requests\FilterNameRequest;
use App\Http\Requests\FilterPriceRequest;
use App\Http\Requests\FilterRoomsRequest;
use PHPUnit\Framework\TestCase;

class ApartmentControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return void
     */
    public function testRulesName()
    {
        $this->assertEquals([
                'name' => 'required|string'
            ],
            getRulesFormRequests(new FilterNameRequest())
        );
    }

    /**
     * @return void
     */
    public function testRulesPrice()
    {
        $this->assertEquals([
                'min' => 'required|integer',
                'max' => 'required|integer',
            ],
            getRulesFormRequests(new FilterPriceRequest())
        );
    }

    /**
     * @return void
     */
    public function testRulesRooms()
    {
        $this->assertEquals([
                'number' => 'required|integer'
            ],
            getRulesFormRequests(new FilterRoomsRequest())
        );
    }
}
